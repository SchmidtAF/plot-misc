# Cython is need for statsmodels - will not be imported through pypi
# cython >= 0.29.33
# pandas>=1.3
# numpy>=1.21
# scipy>=1.5.*
# statsmodels>=0.12.*
# pyarrow fails - is a future dependency of pandas, might be packed with that. 
# pyarrow
pytest>=6
pytest-mock>=3
pytest-dependency>=0.5
scikit-learn>=1.4
seaborn>=0.11
matplotlib>=3.5
bump2version>=1
adjustText>=0.7
jupyter
