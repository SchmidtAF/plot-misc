'''
Constants used throughout the plot-misc package.
'''
import pandas as pd
import numpy.typing as npt
import numpy as np
from packaging import version
from typing import Any, List, Type, Union, Tuple, Callable

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# NAMES

class TableNames(object):
    '''
    Often used strings
    '''
    phenotype_table = 'phenotype'
    outcome_format  = 'trait'
    exposure        = 'exposure'
    exposure_format = 'exposure_formatted'
    uniprot         = 'uniprot_id'
    uniprot_label   = 'uniprot_display_label'
    plot_label      = 'plot_label'
    pvalue          = 'pvalue'
    pvalue_log10    = 'pvalue_log10'
    file_name       = 'file_name'
    analysis        = 'analysis'
    index           = 'index'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Forest plot
class ForestNames(object):
    s_col                          = 's_col'
    c_col                          = 'c_col'
    a_col                          = 'a_col'
    g_col                          = 'g_col'
    y_col                          = 'y_axis'
    strata_del                     = 'strata_del'
    group_del                      = 'group_del'
    order_col                      = 'order'
    min                            = 'min'
    max                            = 'max'
    mean                           = 'mean'
    fontweight                     = 'bold'
    kwargs                         = 'kwargs'
    span                           = 'span'
    ESTIMATE                       = 'estimate'
    LOWER_BOUND                    = 'lower_bound'
    UPPER_BOUND                    = 'upper_bound'
    PVALUE                         = 'p-value'
    CI                             = 'confidence_interval'
    data_table                     = 'data_table'
    EmpericalSupport_Coverage      = 'coverage'
    EmpericalSupport_Compatability = 'compatibility'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Utils Names
class UtilsNames(object):
    value_input        = 'curated_matrix_value'
    annot_input        = 'curated_matrix_annotation'
    annot_star         = 'matrix_star'
    annot_pval         = 'matrix_pvalue'
    annot_effect       = 'matrix_point_estimate'
    value_point        = 'curated_matrix_point_estimate_value'
    value_original     = 'crude_point_estimate'
    source_data        = 'source_data'
    mat_point          = 'point'
    mat_pvalue         = 'pvalue'
    mat_index          = 'id'
    mat_exposure       = 'exposure'
    mat_outcome        = 'outcome'
    mat_exposure_list  = ['IL2ra', 'IP10', 'SCF', 'TRAIL']
    mat_outcome_list   = ['HDL-C', 'LDL-C']
    mat_annot_star     = 'star'
    mat_annot_pval     = 'pvalues'
    mat_annot_point    = 'point_estimates'
    mat_annot_none     = '`NoneType`'
    roc_false_positive = 'false_positive'
    roc_sensitivity    = 'sensitivity'
    roc_threshold      = 'threshold'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class NamesDecisionCurves(object):
    '''
    Module names for the decision_curve model
    '''
    TP_RATE      = 'True positive rate'
    FP_RATE      = 'False positive rate'
    THRESHOLD    = 'Threshold'
    ALL_MODEL    = 'All model'
    NONE_MODEL   = 'None model'
    MODEL        = 'model'
    NETBENEFIT   = 'Net benefit'
    HARM         = 'harm'
    COL          = 'col'
    LTY          = 'lty'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class NamesMachineLearnig(object):
    '''
    Module names for the machine_learning module
    '''
    DATA           = 'data'
    CI_COLOUR      = 'ci_colour'
    CI_LINEWIDTH   = 'ci_linewidth'
    DOT_COLOUR     = 'dot_colour'
    DOT_MARKER     = 'dot_marker'
    LINE_COLOUR    = 'line_colour'
    LINE_LINEWIDTH = 'line_linewidth'
    LINE_LINESTYLE = 'line_linestyle'

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# CHECKING INPUTS

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# numpy typing
def as_array(a: npt.ArrayLike) -> np.ndarray:
    return np.array(a)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class InputValidationError(Exception):
    pass

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_type(param: Any, types: Union[Tuple[Type], Type],
            param_name: Union[str, None]=None) -> bool:
    """
    Checks if a given parameter matches any of the supplied types
    
    Parameters
    ----------
    param: object to test
    types: either a single type, or a tuple of types to test against.
    param_name: an optional string to return the parameter name in the
        errormessage
    
    Returns
    -------
    True if the parameter is an instance of any of the given types.
    Raises AttributeError otherwise.
    """
    if not isinstance(param, types):
        if param_name is None:
            raise InputValidationError(
                f"Expected any of [{types}], got {type(param)}."
            )
        else:
            raise InputValidationError(
                f"Expected any of [{types}], "
                f"got {type(param)}; Please see parameter: `{param_name}`."
            )
    return True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_df(df: Any) -> bool:
    """
    Checks if object is a pd.DataFrame.
    
    Parameters
    ----------
    df: object
    
    Returns
    -------
    True if the df is a pd.DataFrame. Raises InputValidationError otherwise.
    """
    return is_type(df, pd.DataFrame)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_series_type(column: Union[pd.Series, pd.DataFrame],
                   types: Union[Tuple[Type], Type],
                   ) -> bool:
    """
    Checks if a pd.DataFrame or pd.Series contest has the supplied type.
    
    _Note_: instead of testing the dtypes, the function will look over each
        element and test this individually.
    
    Parameters
    ----------
    column: pd.Series or pd.DataFrame,
    types: a single type.
    
    Returns
    -------
    True if the column(s) match(es) the given types. Raises
    InputValidationError otherwise.
    """
    # check input
    is_type(column, (pd.DataFrame, pd.Series))
    # run tests
    if isinstance(column, pd.Series):
        [is_type(col, types) for col in column]
    elif isinstance(column, pd.DataFrame):
        if version.parse('2.0.3') <= version.parse(pd.__version__):
            # iteritems got depricated.
            column.iteritems = column.items
        for _, col in column.items():
            [is_type(co, types) for co in col]
    # return
    return True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def same_len(object1: Any, object2: Any,
             object_names:Union[List[str], None]=None,
             ) -> bool:
    """
    Check if two object's have the same length, and otherwise raise
    `ValueError`.
    
    Arguments
    ---------
    object1, object2 : Any
        Any type of object.
    objects_names : list of strings
        The two objects the series our sourced from. Will be returned in any
        potential `IndexError` message.
        
    Returns
    -------
    True if all OK. Raises a ValueError otherwise.
    """
    n1 = len(object1)
    n2 = len(object2)
    if object_names is None:
        object_names = ['object1', 'object2']
    elif len(object_names) !=2:
        raise ValueError('`object_names` should be `NoneType` or contain '
                         'two strings')
    # the actual test
    if n1 != n2:
        raise ValueError("The length of `{0}`: {1}, does not match the length "
                         "of `{2}`: {3}.".format(object_names[0], n1,
                                               object_names[1], n2)
                         )
    return True

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def are_columns_in_df(
    df: pd.DataFrame, expected_columns: Union[List[str], str],
    warning: bool=False
) -> bool:
    """
    Checks if all expected columns are present in a given pandas.DataFrame.
    
    Parameters
    ----------
    df: pandas.DataFrame
    expected_columns: either a single column name or a list of column names to test
    warning : bool, default False
        raises a warning instead of an error.
    
    
    Returns
    -------
    True if all expected_columns are in the df. Raises InputValidationError otherwise.
    """
    # constant
    message = "The following columns are missing from the pandas.DataFrame: {}"
    res = True
    # tests
    expected_columns_set: Set[str] = set(expected_columns) if isinstance(
        expected_columns, list
    ) else set([expected_columns])
    
    missing_columns = expected_columns_set - set(df.columns)
    # return
    if missing_columns:
        if warning == False:
            raise InputValidationError(
                message.format(missing_columns)
            )
        else:
            warnings.warn(
                message.format(missing_columns)
            )
            res = False
    return res

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def string_to_list(object:Any) -> Union[Any, List[str]]:
    '''
    Checks if `object` is a string and wraps this in a list, returns the
    original object if it is not a string.
    
    Parameters
    ----------
    object : Any
        Any object that might be a string.
    
    Returns
    -------
    string wrapped in a list or the original object type.
    '''
    if isinstance(object, str):
        return list(object)
    else:
        return object

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _assign_empty_default(arguments:List[Any], empty_object:Callable[[],Any],
                         ) -> List[Any]:
    '''
    Takes a list of `arguments`, checks if these are `NoneType` and if so
    asigns them 'empty_object'.
    
    This function helps deal with the pitfall of assigning an empty mutable
    object as a default function argument, which would persist through multiple
    function calls, leading to unexpected/undesired behaviours.
    
    Parameters
    ----------
    arguments: list of arguments
        A list of arguments which may be set to `NoneType`.
    empty_object: Callable that returns a mutable object
        Examples include a `list` or a `dict`.
    
    Returns
    -------
    new_arguments: list
        List with `NoneType` replaced by empty mutable object.
    
    Examples
    --------
    >>> assign_empty_default(['hi', None, 'hello'], empty_object=list)
    ['hi', [], 'hello']
    '''
    # check input
    is_type(arguments, list, 'arguments')
    is_type(empty_object, type, 'empty_object')
    # loop over arguments
    new_args = [empty_object() if arg is None else arg for arg in arguments]
    # return
    return new_args

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# error messages
class Error_MSG(object):
    '''
    A collection of error messages.
    '''
    MISSING_DF = '`{}` contains missing values.'
    INVALID_STRING = '`{}` should be limited to `{}`.'
    INVALID_EXACT_LENGTH = '`{}` needs to contain exactly {} elements, not {}.'

