
'''
A module to draw and annotate heatmaps using matplotlib or seasborn.

A majority of the code comes from
    `here <https://matplotlib.org/stable/gallery/images_contours_and_fields/image_annotated_heatmap.html>`_.
'''

# modules
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from plot_misc.utils.utils import _update_kwargs
from plot_misc.constants import (
    is_type,
    as_array,
    _assign_empty_default,
)
from typing import Any, List, Type, Union, Tuple, Optional, Dict

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def heatmap(data:Union[pd.DataFrame, as_array],
            row_labels:Union[List[str], as_array],
            col_labels:Union[List[str], as_array],
            grid_col:str='white', grid_linestyle:str='-',
            grid_linewidth:float=3,
            grid_kw:Union[Dict[Any, Any],None]=None,
            cbar_bool:bool=False,
            cbar_label:str="",
            cbar_kw:Union[Dict[Any, Any],None]=None,
            ax:Union[plt.Axes, None]=None,
            **kwargs:Optional[Any]) -> Tuple[matplotlib.image.AxesImage,
                                             matplotlib.colorbar.Colorbar]:
    """
    Creates a heatmap from a numpy array and two lists of labels. The returned
    objects can be used to annotate the cells using for example
    `annotate_heatmap`.
    
    Parameters
    ----------
    data : pd.DataFrame or np.array
        A 2D numpy array of shape (M, N).
    row_labels : list or np.array
        A list or array of length M with the labels for the rows.
    col_labels : list or np.array
        A list or array of length N with the labels for the rows.
    grid_col : str, default 'white'
        The colour of the grid lines
    grid_linestyle : str, default '-'
        The linestyle of the grid lines
    grid_linewidth : float, default 3
        The grid lines width.
    grid_kw : dict
        A dictionary with arguments to `matplotlib.Axes.grid`. Optional.
    cbar_bool : boolean, default False
        Set to True to add a colour bar.
    cbar_label : str
        The label for the colorbar.  Optional.
    cbar_kw : dict
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    ax : plt.Axes, default NoneType
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    *_kw : dict, default None
        Optional arguments supplied to the various plotting functions:
            grid_kw --> ax.grid
            cbar_kw --> ax.figure.colorbar
    **kwargs: Any
        All other arguments are forwarded to `imshow`.
    
    Returns
    -------
    im: mpl.image.AxesImage
    cbar: mpl.colorbar.Colorbar
    """
    
    # create a axes if needed
    if not ax:
        ax = plt.gca()
    # check input
    if isinstance(data, pd.DataFrame):
        matrix = data.copy().to_numpy()
    else:
        matrix = data
    # copy
    row_lab = row_labels
    col_lab = col_labels
    # check additional input
    is_type(row_lab, (list, np.array))
    is_type(col_lab, (list, np.array))
    is_type(cbar_label, str)
    # map None to dict
    grid_kw, cbar_kw = _assign_empty_default([grid_kw, cbar_kw], dict)
    # ### Plot the heatmap
    im = ax.imshow(matrix, **kwargs)
    # Create colorbar
    if cbar_bool == True:
        # NOTE if the kwargs for colobar is extended use `_update_kwargs
        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
        cbar.ax.set_ylabel(cbar_label, rotation=-90, va="bottom")
    else:
        cbar = None
    # Show all ticks and label them with the respective list entries.
    ax.set_xticks(np.arange(matrix.shape[1]))
    ax.set_xticklabels(col_lab)
    ax.set_yticks(np.arange(matrix.shape[0]))
    ax.set_yticklabels(row_lab)
    # Let the horizontal axes labeling appear on top.
    # ax.tick_params(top=True, bottom=False,
    #                labeltop=True, labelbottom=False)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=30, ha="right",
             rotation_mode="anchor")
    # Turn spines off and create white grid.
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    # set tick marks
    ax.set_xticks(np.arange(matrix.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(matrix.shape[0]+1)-.5, minor=True)
    # grid
    new_grid_kwargs = _update_kwargs(
        update_dict=grid_kw, which="minor", color=grid_col,
                     linestyle=grid_linestyle, linewidth=grid_linewidth,
                     )
    ax.grid(**new_grid_kwargs)
    ax.tick_params(which="minor", bottom=False, left=False)
    # return stuff
    return im, cbar

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def annotate_heatmap(im:plt.Axes.imshow,
                     data:Union[pd.DataFrame, as_array, None]=None,
                     valfmt:Union[str, matplotlib.ticker.Formatter, None]=None,
                     textcolors:Union[Tuple[str], List[str]]=("black", "white"),
                     threshold:Union[float,None]=None,
                     **text_kw:Optional[Any],
                     ) -> List[plt.Text]:
    """
    A function to annotate mpl.image.AxesImage object, most typically a
    heatmap generated by `heatmap`.
    
    Parameters
    ----------
    im : plt.Axes.imshow
        The AxesImage to be labeled.
    data : pd.DataFrame or np.array, default `Nonetype`
        A 2D numpy array of shape (M, N). Optional.
    valfmt : str or matplotlib.ticker.Formatter, default `NoneType`
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}" - (note the `x` is needs
        to be included to represent the numerical), or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors: list or tuple of string, default `('black', 'white')`
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.
    threshold: float, default `NoneType`
        The absolute value in data units according to which the colors from
        textcolors are applied.  If None (the default) uses the middle of the
        colormap as separation.  Optional.
    **text_kw: Any
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    
    Returns
    -------
    texts: list of plt.text.Text objects
        The plt.text.Text objects contain `tuple`-like objects with the
        Cartesian-coordinates and the text for each coordinate.
    """
    
    # mapping data to matrix
    values = im.get_array()
    if data is None:
        matrix = im.get_array()
    elif isinstance(data, pd.DataFrame):
        matrix = data.copy().to_numpy()
    else:
        matrix = data
    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        # this will value is matrix is a string
        try:
            threshold = im.norm(values.max())/2.
        except np.core._exceptions.UFuncTypeError:
            threshold = None
    # Set default alignment to center, but allow it to be
    # overwritten by text_kw.
    kw = _update_kwargs(update_dict=text_kw,
                        horizontalalignment="center",
                        verticalalignment="center",
                        )
    # Get the formatter in case a string is supplied
    if not valfmt is None:
        if isinstance(valfmt, str):
            valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)
    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(matrix.shape[0]):
        for j in range(matrix.shape[1]):
            # only run if threshold exists
            if not threshold is None:
                kw.update(color=textcolors[int(im.norm(abs(values[i, j])) > threshold)])
            # format text or not
            if not valfmt is None:
                text = im.axes.text(j, i, valfmt(matrix[i, j], None), **kw)
            else:
                text = im.axes.text(j, i, matrix[i,j], **kw)
            texts.append(text)
    # returning stuff
    return texts

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def clustermap(data:pd.DataFrame,
               cmap:matplotlib.cm.get_cmap=matplotlib.colormaps.get_cmap('Spectral'),
               annot:Union[pd.DataFrame, None]=None,
               fsize:Tuple[float]=(15, 15),
               linewidths:float=1.0,
               cpos:Union[None, Tuple[float]]=(0.09, 0.02, 0.03, 0.10),
               annotsize:float=6, clab:str='', clabfs:float=7,
               fmt:str=".3",
               clabpos:str='left', clabtsize:float=5,
               xticklabsize:float=8, yticklabsize:float=6,
               yticks:bool=True, xticks:bool=True,
               cbar_dict_kw:Union[Dict[Any, Any],None]=None,
               tree_dict_kw:Union[Dict[Any, Any],None]=None,
               annot_dict_kw:Union[Dict[Any, Any],None]=None,
               clustermap_dict_kw:Union[Dict[Any, Any],None]=None,
               ) -> sns.matrix.ClusterGrid:
    '''
    Creates a heatmap where the rows and columns are clustered based on
    the plotted values.
    Provides a wrap of `sns.clustermap`.
    
    Parameters
    ----------
    data : pd.DataFrame
        A datafarme of shape (M, N).
    fsize : tuple of two floats, default `(15, 15)`
        figure size in cm
    linewidths : float, default 1.0
        Width of the lines that will divide each cell.
    annot : pd.DataFrame, default `NoneType`
        An opional dataframe used for annotation.
    annotsize : float, default 6.0
        The fontsize of the annotations, will be parsed to
        `matplotlib.axes.Axes.text`.
    fmt : string, default '.3'
        String formatting code to use when adding annotations.
    cmap : matplotlib.colormaps, default 'viridis'
        matplotlib colormaps
    cpos : tuple of four floats
        Position of the colorbar axes in the figure
        (left, bottom, width, height). Set to None to disable colour bar.
    clab : str, default ' '
        colour guide y-axis title.
    clabsf : float, default 7.0.
        The title fontsize
    clabpos : str, `left`
        Position of the title.
    clabtsize : float, default 5.0
        colour guide tick label fontsize.
    xticklabsize : float, default 8.0
        The x-axis tick label fontsize.
    yticklabsize : float, default 6.0
        The y-axis tick label fontsize.
    yticks : boolean, default `True`
        Whether the yticks should be plotted.
    xticks : boolean, default `True`
        Whether the xticks should be plotted.
    *_kw : dict, default None
        Optional arguments supplied to the various plotting functions:
            cbar_dict_kw        --> matplotlib.figure.Figure.colorbar
            tree_dict_kw        --> matplotlib.collections.LineCollection
            annot_dict_kw       --> matplotlib.axes.Axes.text
            clustermap_dict_kw  --> sns.clustermap
    
    Returns
    -------
    cm: sns.matrix.ClusterGrid
    '''
    # #### constants
    # inches to cm's
    FSCALE=0.393700787
    # map None to dict
    cbar_dict_kw, tree_dict_kw, annot_dict_kw, clustermap_dict_kw =\
        _assign_empty_default(
            [cbar_dict_kw, tree_dict_kw, annot_dict_kw, clustermap_dict_kw],
            dict)
    # update keyword dictionaries
    annot_kw = _update_kwargs(update_dict=annot_dict_kw,
                              size=annotsize,
                              )
    clustermap_kw = _update_kwargs(update_dict=clustermap_dict_kw,
                                   fmt=fmt, linewidths=linewidths,
                                   figsize=(fsize[0] * FSCALE, fsize[1] * FSCALE),
                                   cbar_pos=cpos, cmap=cmap,
                                   annot=annot,
                                   )
    # make figure
    cm = sns.clustermap(data,
                        cbar_kws=cbar_dict_kw,
                        tree_kws=tree_dict_kw,
                        annot_kws=annot_kw,
                        **clustermap_kw,
                        )
    # cbar labels
    cm.ax_cbar.axes.yaxis.set_label_text(clab, fontsize=clabfs)
    cm.ax_cbar.axes.yaxis.set_label_position(clabpos)
    cm.ax_cbar.tick_params(labelsize=clabtsize)
    # heatmap tick labels
    cm.ax_heatmap.set_xticklabels(cm.ax_heatmap.get_xmajorticklabels(),
                                   fontsize = xticklabsize)
    cm.ax_heatmap.set_yticklabels(cm.ax_heatmap.get_ymajorticklabels(),
                                   fontsize = yticklabsize)
    # removing axis labels
    cm.ax_heatmap.set_ylabel("")
    cm.ax_heatmap.set_xlabel("")
    # add both xy ticks
    cm.ax_heatmap.tick_params('both', reset=False, bottom=xticks, right=yticks)
    # return
    return cm

